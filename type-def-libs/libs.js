declare module "apollo-boost" {
  declare module.exports: any;
}

declare module "@expo/vector-icons" {
  declare module.exports: any;
}

declare module "@apollo/react-hooks" {
  declare module.exports: any;
}

declare module "native-base" {
  declare module.exports: any;
}

declare module "react-intl" {
  declare module.exports: any;
}

declare module "react-native" {
  declare module.exports: any;
}

declare module "react-navigation" {
  declare module.exports: any;
}

declare module "styled-components" {
  declare module.exports: any;
}

declare module "graphql-tag" {
  declare module.exports: any;
}
