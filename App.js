// @flow
import React, { useState, useEffect } from "react";
import ApolloClient from "apollo-boost";
import { ApolloProvider } from "@apollo/react-hooks";
import { IntlProvider } from "react-intl";
import { Root as NativeBaseRoot } from "native-base";
import Root from "./src/root";
import { NavigationService } from "./src/utils";
import Messages from "./src/intl/fr";
import AppConfig from "./app.json";

const client = token => {
  const config = {
    uri: `${AppConfig.config.apiUrl}/graphql`,
    fetchOptions: {
      credentials: "include"
    },
    credentials: "include"
  };
  return new ApolloClient(config);
};

const App = () => (
  <ApolloProvider client={client()}>
    <IntlProvider locale="fr" messages={Messages}>
      <NativeBaseRoot>
        <Root
          ref={navigatorRef => {
            // Passing the top-level (root) navigator ref to our NavigationService
            // allows us to access the navigation without the navigation props.
            NavigationService.setTopLevelNavigator(navigatorRef);
          }}
        />
      </NativeBaseRoot>
    </IntlProvider>
  </ApolloProvider>
);

export default App;
