// @flow
import React from "react";
import ApolloClient from "apollo-boost";
import { AsyncStorage } from "react-native";
import { ApolloProvider } from "@apollo/react-hooks";
import { createBottomTabNavigator } from "react-navigation-tabs";
import { Icon } from "./components/base-components";

import Adverts from "./adverts";
import Publishment from "./publishment";
import Profile from "./profile";
import News from "./news";
import AppConfig from "../app.json";
import theme from "./theme";

const UsersNavigation = createBottomTabNavigator(
  {
    Adverts: {
      screen: Adverts,
      navigationOptions: {
        title: "Annonces",
        tabBarOptions: {
          showLabel: false
        },
        tabBarIcon: ({ focused }) => (
          <Icon
            name="md-list"
            size="middle"
            color={focused ? theme.colors.primary : theme.colors.text}
          />
        )
      }
    },
    Publishment: {
      screen: Publishment,
      navigationOptions: {
        title: "Add",
        tabBarOptions: {
          showLabel: false
        },
        tabBarIcon: ({ focused }) => (
          <Icon
            name="md-add-circle-outline"
            size="middle"
            color={focused ? theme.colors.primary : theme.colors.text}
          />
        )
      }
    },
    News: {
      screen: News,
      navigationOptions: {
        title: "News",
        tabBarOptions: {
          showLabel: false
        },
        tabBarIcon: ({ focused }) => (
          <Icon
            name="md-notifications-outline"
            size="middle"
            color={focused ? theme.colors.primary : theme.colors.text}
          />
        )
      }
    },
    Profile: {
      screen: Profile,
      navigationOptions: {
        title: "Profile",
        tabBarOptions: {
          showLabel: false
        },
        tabBarIcon: ({ focused }) => (
          <Icon
            name="md-person"
            size="middle"
            color={focused ? theme.colors.primary : theme.colors.text}
          />
        )
      }
    }
  },
  {
    initialRouteName: "Adverts"
  }
);

type Props = {
  navigation: Object
};

type State = {
  token: ?string
};

class App extends React.Component<Props, State> {
  static router = UsersNavigation.router;
  state = {
    token: null
  };
  componentDidMount() {
    const getAccessToken = async () => {
      const access_token = await AsyncStorage.getItem("access_token");
      if (access_token) {
        this.setState({ token: access_token });
      } else {
        this.props.navigation.navigate({ routeName: "Login", key: null });
      }
    };
    getAccessToken();
  }

  client = (token: string) => {
    const config = {
      uri: `${AppConfig.config.apiUrl}/graphql`,
      fetchOptions: {
        credentials: "include"
      },
      credentials: "include",
      headers: {
        authorization: `bearer ${token}`
      }
    };
    return new ApolloClient(config);
  };

  render() {
    const { navigation } = this.props;
    return (
      <ApolloProvider
        client={this.state.token && this.client(this.state.token)}
      >
        <UsersNavigation navigation={navigation} />
      </ApolloProvider>
    );
  }
}

export default App;
