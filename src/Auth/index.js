// @flow
import React from "react";
import { createBottomTabNavigator } from "react-navigation-tabs";
import { Ionicons } from "@expo/vector-icons";
import { Icon } from "../components/base-components";
import theme from "../theme";
import Login from "./login";
import Register from "./register";

const SignNavigation = createBottomTabNavigator(
  {
    Login: {
      screen: Login,
      navigationOptions: ({ navigation }) => {
        const { params } = navigation.state;
        return {
          title: "Se connecter",
          tabBarOptions: {
            showLabel: false
          },
          tabBarVisible: params && params.hide ? false : true,
          tabBarIcon: ({ focused }) => (
            <Icon
              name="md-log-in"
              size="middle"
              color={focused ? theme.colors.primary : theme.colors.text}
            />
          )
        };
      }
    },
    Register: {
      screen: Register,
      navigationOptions: ({ navigation }) => {
        const { params } = navigation.state;
        return {
          title: "S'inscrire",
          tabBarOptions: {
            showLabel: false
          },
          tabBarVisible: params && params.hide ? false : true,
          tabBarIcon: ({ focused }) => (
            <Icon
              name="md-person-add"
              size="middle"
              color={focused ? theme.colors.primary : theme.colors.text}
            />
          )
        };
      }
    }
  },
  {
    initialRouteName: "Login"
  }
);

export default SignNavigation;
