// @flow
import React, { useState } from "react";
import { TouchableOpacity } from "react-native";
import { FormattedMessage } from "react-intl";
import { Item, Input, Button, Label, Toast } from "native-base";
import { Text, Padded } from "../components/base-components";
import Layout from "../layout";
import AppConfig from "../../app.json";

const ForgotPassword = ({ navigation }: { navigation: Object }) => {
  const [email, setEmail] = useState(null);
  const sendingPasswordLink = async email =>
    await fetch(`${AppConfig.config.apiUrl}/auth/forgot-password`, {
      method: "POST",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json"
      },
      body: JSON.stringify({
        email,
        url: `${AppConfig.config.apiUrl}/admin/plugins/users-permissions/auth/reset-password`
      })
    }).then(result => result.json());

  const displaySuccessMessage = () =>
    Toast.show({
      text: <FormattedMessage id="auth.emailSend" />,
      position: "bottom",
      duration: 3000,
      type: "success",
      textStyle: {
        textAlign: "center"
      }
    });
  return (
    <Layout
      backNav={() => navigation.navigate({ routeName: "Login", key: null })}
      title={<FormattedMessage id="auth.forgotPassword" />}
    >
      <Item floatingLabel>
        <Label>
          <FormattedMessage id="auth.email" />
        </Label>
        <Input
          keyboardType="email-address"
          onChangeText={text => setEmail(text.toLowerCase())}
        />
      </Item>
      <Padded top />
      <Button
        block
        disabled={!email}
        onPress={() =>
          sendingPasswordLink(email).then(async res => {
            displaySuccessMessage();
            navigation.navigate({ routeName: "Login", key: null });
          })
        }
      >
        <Text bold disabled={!email} color="white">
          <FormattedMessage id="auth.send" />
        </Text>
      </Button>
    </Layout>
  );
};

export default ForgotPassword;
