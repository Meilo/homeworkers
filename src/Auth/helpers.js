// @flow
import AppConfig from "../../app.json";

export const logger = async (data: { identifier: string, password: string }) =>
  await fetch(`${AppConfig.config.apiUrl}/auth/local`, {
    method: "POST",
    headers: {
      Accept: "application/json",
      "Content-Type": "application/json"
    },
    body: JSON.stringify(data)
  }).then(result => result.json());

export const emailValidation = (email: string) => {
  const regex = /\S+@\S+\.\S+/;
  return regex.test(email);
};
