// @flow
import React, { useState } from "react";
import { AsyncStorage, TouchableOpacity } from "react-native";
import { Item, Input, Button, Label, Content } from "native-base";
import { FormattedMessage } from "react-intl";
import Layout from "../layout";
import AppConfig from "../../app.json";
import {
  Padded,
  Text,
  Loader,
  displayToast
} from "../components/base-components";
import ForgotPassword from "./forgotPassword";
import { logger } from "./helpers";

const Login = ({ navigation }: { navigation: Object }) => {
  const [loginData, setLoginData]: [
    Object,
    (data: Object) => Object
  ] = useState({});
  const [loading, setLoading] = useState(false);
  const isCompleted = loginData.identifier && loginData.password;

  return !loading ? (
    <Layout title={<FormattedMessage id="auth.connection" />}>
      <Item floatingLabel>
        <Label>
          <FormattedMessage id="auth.identifier" />
        </Label>
        <Input
          autoComplete={false}
          onChangeText={text =>
            setLoginData({ ...loginData, identifier: text.toLowerCase() })
          }
        />
      </Item>
      <Padded top size="small" />
      <Item floatingLabel>
        <Label>
          <FormattedMessage id="auth.password" />
        </Label>
        <Input
          secureTextEntry
          onChangeText={text => setLoginData({ ...loginData, password: text })}
        />
      </Item>
      <Padded top size="standard" />
      <Button
        block
        disabled={!isCompleted}
        onPress={() => {
          setLoading(true);
          navigation.setParams({ hide: true });
          logger(loginData).then(async res => {
            if (res.error) {
              displayToast({
                text: <FormattedMessage id="auth.wrong" />,
                type: "danger"
              });
              setLoading(false);
              navigation.setParams({ hide: false });
              return null;
            }
            await AsyncStorage.setItem("access_token", res.jwt);
            navigation.navigate({ routeName: "App", key: null });
          });
        }}
      >
        <Text bold disabled={!isCompleted} color="white">
          <FormattedMessage id="auth.login" />
        </Text>
      </Button>
      <Padded top size="standard" />
      <TouchableOpacity
        onPress={() =>
          navigation.navigate({ routeName: "ForgotPassword", key: null })
        }
      >
        <Text textAlign="center">
          <FormattedMessage id="auth.forgotPassword" />
        </Text>
      </TouchableOpacity>
    </Layout>
  ) : (
    <Loader />
  );
};

export default Login;
