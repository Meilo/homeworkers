// @flow
import React from "react";
import { AsyncStorage, View, TouchableOpacity } from "react-native";
import { FormattedMessage } from "react-intl";
import { Button } from "native-base";
import { Text, Padded } from "../components/base-components";

const Logout = ({ navigation }: { navigation: Object }) => {
  const logout = async () => {
    await AsyncStorage.removeItem("access_token");
    const token = await AsyncStorage.getItem("access_token");
    if (!token) navigation.navigate({ routeName: "Login", key: null });
  };
  return (
    <View>
      <Button light onPress={logout}>
        <Padded left right>
          <Text bold>
            <FormattedMessage id="auth.logout" />
          </Text>
        </Padded>
      </Button>
    </View>
  );
};

export default Logout;
