// @flow
import React, { useState } from "react";
import { AsyncStorage } from "react-native";
import { useMutation } from "@apollo/react-hooks";
import { gql } from "apollo-boost";
import { FormattedMessage } from "react-intl";
import { Item, Input, Button, Label, Toast, Picker } from "native-base";
import {
  Padded,
  Text,
  Icon,
  Loader,
  displayToast
} from "../components/base-components";
import Layout from "../layout";
import departments from "../utils/departments.json";
import AppConfig from "../../app.json";
import { logger, emailValidation } from "./helpers";

const ADD_USER = gql`
  mutation CreateUser($input: createUserInput) {
    createUser(input: $input) {
      user {
        id
        username
      }
    }
  }
`;

const Register = ({ navigation }: { navigation: Object }) => {
  const [selectedKind, setSelectedKind] = useState(null);
  const [selectedDepartment, setSelectedDepartment] = useState(null);
  const [loading, setLoading] = useState(false);
  const [registerData, setRegisterData]: [
    Object,
    (data: Object) => Object
  ] = useState({});
  const [addUser, { data }] = useMutation(ADD_USER, {
    onCompleted() {
      logger({
        identifier: registerData.username,
        password: registerData.password
      }).then(async res => {
        await AsyncStorage.setItem("access_token", res.jwt);
        navigation.navigate({ routeName: "App", key: null });
      });
    },
    onError() {
      setLoading(false);
      navigation.setParams({ hide: false });
      displayToast({
        text: <FormattedMessage id="auth.emailExist" />,
        type: "danger"
      });
    }
  });

  const isCompleted =
    registerData.username &&
    registerData.password &&
    registerData.email &&
    registerData.kind &&
    registerData.department;
  return !loading ? (
    <Layout title={<FormattedMessage id="auth.registration" />}>
      <Item floatingLabel>
        <Label>
          <FormattedMessage id="auth.identifier" />
        </Label>
        <Input
          autoComplete={false}
          onChangeText={text =>
            setRegisterData({
              ...registerData,
              username: text.toLowerCase().replace(/\s+/g, "")
            })
          }
          value={registerData.username}
        />
      </Item>
      <Padded top size="small" />
      <Item floatingLabel>
        <Label>
          <FormattedMessage id="auth.email" />
        </Label>
        <Input
          autoComplete={false}
          keyboardType="email-address"
          onChangeText={text =>
            setRegisterData({ ...registerData, email: text })
          }
          value={registerData.email}
        />
      </Item>
      <Padded top size="small" />
      <Item floatingLabel>
        <Label>
          <FormattedMessage id="auth.password" />
        </Label>
        <Input
          secureTextEntry
          onChangeText={text =>
            setRegisterData({ ...registerData, password: text })
          }
        />
      </Item>
      <Padded top />
      <Item picker>
        <Label>
          <FormattedMessage id="auth.kind" />
        </Label>
        <Picker
          mode="dropdown"
          iosHeader={<FormattedMessage id="auth.yours" />}
          headerBackButtonText={<Icon name="md-arrow-back" size="large" />}
          style={{ width: 300 }}
          placeholder={<FormattedMessage id="auth.yours" />}
          selectedValue={selectedKind}
          onValueChange={value => {
            setSelectedKind(value);
            setRegisterData({ ...registerData, kind: value });
          }}
        >
          <Picker.Item
            label={<FormattedMessage id="auth.male" />}
            value="male"
          />
          <Picker.Item
            label={<FormattedMessage id="auth.female" />}
            value="female"
          />
        </Picker>
      </Item>
      <Padded top />
      <Item picker>
        <Label>
          <FormattedMessage id="auth.county" />
        </Label>
        <Picker
          mode="dropdown"
          iosHeader={<FormattedMessage id="auth.county" />}
          headerBackButtonText={<Icon name="md-arrow-back" size="large" />}
          style={{ width: 300 }}
          placeholder={<FormattedMessage id="auth.choose" />}
          selectedValue={selectedDepartment}
          onValueChange={value => {
            setSelectedDepartment(value);
            setRegisterData({ ...registerData, department: value });
          }}
        >
          {departments.map(department => (
            <Picker.Item
              label={`${department.code} - ${department.name}`}
              value={department.code}
              key={department.id}
            />
          ))}
        </Picker>
      </Item>
      <Padded top />
      <Button
        block
        disabled={!isCompleted}
        onPress={() => {
          if (emailValidation(registerData.email)) {
            setLoading(true);
            navigation.setParams({ hide: true });
            addUser({
              variables: {
                input: {
                  data: {
                    ...registerData
                  }
                }
              }
            });
          } else {
            displayToast({
              text: <FormattedMessage id="auth.emailNotValide" />,
              type: "danger"
            });
          }
        }}
      >
        <Text bold color="white">
          <FormattedMessage id="auth.registered" />
        </Text>
      </Button>
    </Layout>
  ) : (
    <Loader />
  );
};

export default Register;
