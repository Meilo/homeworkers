// @flow
import React, { useState, useEffect } from "react";
import { AsyncStorage, View, Text } from "react-native";
import { FormattedMessage } from "react-intl";
import Layout from "../layout";

const Publishment = ({ navigation }: { navigation: Object }) => {
  return (
    <Layout
      title={<FormattedMessage id="publishment.header" />}
      navigation={navigation}
    >
      <View>
        <Text>Publishment</Text>
      </View>
    </Layout>
  );
};

export default Publishment;
