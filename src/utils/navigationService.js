// @flow
import { NavigationActions } from "react-navigation";

let navigator;

// Setting the navigator
function setTopLevelNavigator(navigatorRef: any) {
  navigator = navigatorRef;
}

// Expose navigation functions we need
// We can add others navigation functions like dispatch, goBack, getParam, etc
function navigate(routeName: string, params?: Object) {
  navigator.dispatch(
    NavigationActions.navigate({
      routeName,
      params
    })
  );
}

export default {
  navigate,
  setTopLevelNavigator
};
