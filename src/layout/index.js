// @flow
import React from "react";
import { TouchableOpacity } from "react-native";
import {
  Container,
  Header,
  Body,
  Content,
  Title,
  Left,
  Right
} from "native-base";
import { Padded, Icon } from "../components/base-components";

const Layout = ({
  children,
  title,
  backNav,
  navigation
}: {
  children: Object,
  title: Object,
  backNav?: string,
  navigation: Object
}) => (
  <Container>
    <Header>
      {backNav && (
        <Left>
          <TouchableOpacity onPress={backNav}>
            <Icon name="md-arrow-back" size="large" />
          </TouchableOpacity>
        </Left>
      )}
      <Body>
        <Title>{title}</Title>
      </Body>
      {backNav && <Right />}
    </Header>
    <Content>
      <Padded left top bottom right>
        {children}
      </Padded>
    </Content>
  </Container>
);

export default Layout;
