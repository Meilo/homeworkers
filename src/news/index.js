// @flow
import React, { useState, useEffect } from "react";
import { AsyncStorage, View, Text } from "react-native";
import { FormattedMessage } from "react-intl";
import Layout from "../layout";

const News = ({ navigation }: { navigation: Object }) => {
  return (
    <Layout
      title={<FormattedMessage id="news.header" />}
      navigation={navigation}
    >
      <View>
        <Text>News</Text>
      </View>
    </Layout>
  );
};

export default News;
