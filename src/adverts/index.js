// @flow
import React, { useState, useEffect } from "react";
import { AsyncStorage, View, Text } from "react-native";
import { FormattedMessage } from "react-intl";
import Layout from "../layout";

const Adverts = ({ navigation }: { navigation: Object }) => {
  return (
    <Layout
      title={<FormattedMessage id="adverts.header" />}
      navigation={navigation}
    >
      <View>
        <Text>Adverts</Text>
      </View>
    </Layout>
  );
};

export default Adverts;
