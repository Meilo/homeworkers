// @flow

const theme = {
  colors: {
    lightGrey: "#EEEEEE",
    text: "#333333",
    primary: "#1e90ff",
    white: "white"
  },
  padding: {
    tiny: 2,
    verySmall: 5,
    small: 10,
    standard: 20,
    middle: 25,
    large: 30,
    veryLarge: 50,
    extraLarge: 70
  },
  fontSize: {
    small: 10,
    standard: 15,
    large: 25
  }
};

export default theme;
