// @flow
import { createSwitchNavigator, createAppContainer } from "react-navigation";

import Login from "./auth";
import ForgotPassword from "./auth/forgotPassword";
import App from "./app";

const Root = createSwitchNavigator(
  {
    App: { screen: App },
    Login: { screen: Login },
    ForgotPassword: { screen: ForgotPassword }
  },
  {
    initialRouteName: "App"
  }
);

export default createAppContainer(Root);
