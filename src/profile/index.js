// @flow
import React, { useState, useEffect } from "react";
import { Query } from "react-apollo";
import { useMutation } from "@apollo/react-hooks";
import { Item, Input, Label, Picker, Button } from "native-base";
import gql from "graphql-tag";
import { View } from "react-native";
import { FormattedMessage } from "react-intl";
import {
  Flex,
  Padded,
  Text,
  Loader,
  Icon,
  displayToast
} from "../components/base-components";
import Layout from "../layout";
import Logout from "../auth/logout";
import Avatar from "./avatar";
import { upper } from "./helpers";
import departments from "../utils/departments.json";
import { ViewerConsumer } from "../components/viewer/viewerProvider";
import ViewerContext from "../components/viewer/viewerContext";

const USER = gql`
  query getUser($id: ID!) {
    user(id: $id) {
      username
      email
      kind
      department
    }
  }
`;

const EDIT_USER = gql`
  mutation UpdateUser($input: updateUserInput) {
    updateUser(input: $input) {
      user {
        username
        email
        kind
        department
      }
    }
  }
`;

const Profile = ({ navigation }: { navigation: Object }) => {
  const [editUser] = useMutation(EDIT_USER, {
    onCompleted(res) {
      displayToast({
        text: <FormattedMessage id="profile.editSuccess" />,
        type: "success"
      });
    },
    onError() {
      displayToast({
        text: <FormattedMessage id="auth.emailExist" />,
        type: "danger"
      });
    }
  });

  const [userInfos, setUserInfos] = useState({});
  const [selectedKind, setSelectedKind] = useState(null);
  const [selectedDepartment, setSelectedDepartment] = useState(null);

  return (
    <Layout
      title={<FormattedMessage id="profile.header" />}
      navigation={navigation}
    >
      <ViewerContext>
        <ViewerConsumer>
          {viewer =>
            viewer && viewer.me ? (
              <Query query={USER} variables={{ id: viewer.me.id }}>
                {({ loading, error, data, refetch }) => {
                  if (loading || error) return <Loader />;
                  return (
                    <View>
                      <Flex>
                        <Avatar />
                        <Padded left />
                        <View>
                          <Text size="large">{upper(data.user.username)}</Text>
                          <Padded bottom size="small" />
                          <Logout navigation={navigation} />
                        </View>
                      </Flex>
                      <Padded top size="veryLarge" />
                      <Flex>
                        <Item floatingLabel>
                          <Label>
                            <FormattedMessage id="auth.email" />
                          </Label>
                          <Input
                            autoComplete={false}
                            keyboardType="email-address"
                            onChangeText={text =>
                              setUserInfos({ ...data.user, email: text })
                            }
                            value={data.user.email}
                          />
                        </Item>
                      </Flex>
                      <Item picker>
                        <Label>
                          <FormattedMessage id="auth.kind" />
                        </Label>
                        <Picker
                          mode="dropdown"
                          iosHeader={<FormattedMessage id="auth.yours" />}
                          headerBackButtonText={
                            <Icon name="md-arrow-back" size="large" />
                          }
                          style={{ width: 322 }}
                          placeholder={<FormattedMessage id="auth.yours" />}
                          selectedValue={selectedKind || data.user.kind}
                          onValueChange={value => {
                            setSelectedKind(value);
                            setUserInfos({ ...data.user, kind: value });
                          }}
                        >
                          <Picker.Item
                            label={<FormattedMessage id="auth.male" />}
                            value="male"
                          />
                          <Picker.Item
                            label={<FormattedMessage id="auth.female" />}
                            value="female"
                          />
                        </Picker>
                      </Item>
                      <Item picker>
                        <Label>
                          <FormattedMessage id="auth.county" />
                        </Label>
                        <Picker
                          mode="dropdown"
                          iosHeader={<FormattedMessage id="auth.county" />}
                          headerBackButtonText={
                            <Icon name="md-arrow-back" size="large" />
                          }
                          style={{ width: 270 }}
                          placeholder={<FormattedMessage id="auth.choose" />}
                          selectedValue={
                            selectedDepartment || data.user.department
                          }
                          onValueChange={value => {
                            setSelectedDepartment(value);
                            setUserInfos({
                              ...data.user,
                              department: value
                            });
                          }}
                        >
                          {departments.map(department => (
                            <Picker.Item
                              label={`${department.code} - ${department.name}`}
                              value={department.code}
                              key={department.id}
                            />
                          ))}
                        </Picker>
                      </Item>

                      <Padded top />
                      <Button
                        block
                        onPress={() =>
                          editUser({
                            variables: {
                              input: {
                                where: {
                                  id: viewer.me.id
                                },
                                data: {
                                  email: userInfos.email,
                                  kind: userInfos.kind,
                                  department: userInfos.department
                                }
                              }
                            }
                          })
                        }
                      >
                        <Text bold color="white">
                          <FormattedMessage id="profile.edit" />
                        </Text>
                      </Button>
                    </View>
                  );
                }}
              </Query>
            ) : (
              <Loader />
            )
          }
        </ViewerConsumer>
      </ViewerContext>
    </Layout>
  );
};

export default Profile;
