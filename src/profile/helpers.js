// @flow

export const upper = (lower: string) => {
  const text = lower.charAt(0).toUpperCase() + lower.substring(1);
  return text;
};
