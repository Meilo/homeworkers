// @flow
import React from "react";
import { View, Image } from "react-native";
import { MaterialIcons } from "@expo/vector-icons";

const Avatar = () => (
  <View>
    <MaterialIcons name="account-circle" size={85} />
  </View>
);

export default Avatar;
