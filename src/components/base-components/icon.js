// @flow
import React from "react";
import { Ionicons } from "@expo/vector-icons";
import theme from "../../theme";

const Icon = ({
  name,
  size = "standard",
  color = theme.colors.text
}: {
  name: string,
  size?: string,
  color?: string
}) => <Ionicons name={name} size={theme.padding[size]} color={color} />;

export default Icon;
