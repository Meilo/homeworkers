// @flow
import React from "react";
import { Spinner, Content } from "native-base";
import styled from "styled-components";
import theme from "../../theme";

const Container = styled.View`
  background-color: #ffffff;
  width: 100%;
`;

const Loader = () => (
  <Content>
    <Container>
      <Spinner color="blue" />
    </Container>
  </Content>
);

export default Loader;
