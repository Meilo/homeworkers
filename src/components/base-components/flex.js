// @flow
import React from "react";
import styled from "styled-components";
import theme from "../../theme";

const Flex = styled.View`
  flex-wrap: nowrap;
  align-items: ${props => props.alignItem || "center"};
  justify-content: ${props => props.justifyContent || "flex-start"};
  flex-direction: row;
  flex-grow: ${props => props.flexGrow || 0};
`;

export default Flex;
