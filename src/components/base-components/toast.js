// @flow
import { Toast as RowToast } from "native-base";

const displayToast = ({
  text,
  type,
  position = "bottom"
}: {
  text: Object,
  type: string,
  position?: string
}) =>
  RowToast.show({
    text,
    position,
    duration: 3000,
    type,
    textStyle: {
      textAlign: "center"
    }
  });

export default displayToast;
