// @flow
import React from "react";
import styled from "styled-components";
import theme from "../../theme";

const Text = styled.Text`
  font-weight: ${props => (props.bold ? `bold` : `normal`)};
  font-size: ${props => theme.fontSize[props.size] || theme.fontSize.standard};
  text-align: ${props => props.textAlign || `left`};
  color: ${props =>
    props.disabled
      ? theme.colors.lightGrey
      : theme.colors[props.color] || theme.colors.text};
`;

export default Text;
