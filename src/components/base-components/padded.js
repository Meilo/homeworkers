// @flow
import React from "react";
import styled from "styled-components";
import theme from "../../theme";

const Padded = styled.View`
  padding-left: ${props =>
    props.left
      ? `${theme.padding[props.size] || theme.padding.standard}px`
      : `0px`};
  padding-top: ${props =>
    props.top
      ? `${theme.padding[props.size] || theme.padding.standard}px`
      : `0px`};
  padding-right: ${props =>
    props.right
      ? `${theme.padding[props.size] || theme.padding.standard}px`
      : `0px`};
  padding-bottom: ${props =>
    props.bottom
      ? `${theme.padding[props.size] || theme.padding.standard}px`
      : `0px`};
`;

export default Padded;
