// @flow
import * as React from "react";

const ViewerContext = React.createContext({
  me: {
    id: 0
  }
});

const ViewerProvider = ({
  children,
  value
}: {
  children: React.Node,
  value: Object
}) => <ViewerContext.Provider value={value}>{children}</ViewerContext.Provider>;

const ViewerConsumer = ({
  children
}: {
  children: (value: Object) => React.Node
}) => <ViewerContext.Consumer>{children}</ViewerContext.Consumer>;

export { ViewerConsumer, ViewerProvider };
