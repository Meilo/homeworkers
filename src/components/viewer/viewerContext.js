// @flow
import * as React from "react";
import { Query } from "react-apollo";
import gql from "graphql-tag";
import { ViewerProvider } from "./viewerProvider";

const VIEWER = gql`
  query {
    me {
      id
    }
  }
`;

type Props = {
  children: React.Node
};

const ViewerContext = ({ children }: Props) => (
  <Query query={VIEWER}>
    {({ loading, data }) => {
      if (!data || loading) return children;
      return <ViewerProvider value={data}>{children}</ViewerProvider>;
    }}
  </Query>
);

export default ViewerContext;
